package com.suppliermanagement.controllers;

import com.common.BaseEnum;

/**
 * Based on 'Myscreens' Created by gayashan on 8/14/2017.
 */
public enum SupplierScreens implements BaseEnum {

    PURCHASE_SCREEN("purchase", "/com/suppliermanagement/supView_Purchase.fxml"),
    DASHBOARD_SCREEN("dashboard", "/com/suppliermanagement/suplyView_Dashboard.fxml"),
    SUPPLIER_SCREEN("suppliers", "/com/suppliermanagement/supView_Suppliers.fxml");


    String path;
    String id;

    SupplierScreens(String id, String path) {
        this.path = path;
        this.id = id;
    }

    ;

    public String getPath() {
        return path;
    }

    public String getId() {
        return id;
    }
}
