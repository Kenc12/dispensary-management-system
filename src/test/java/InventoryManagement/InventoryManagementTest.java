package InventoryManagement;

import com.EntityClasses.Machine;
import com.EntityClasses.Supplier;
import com.EntityClasses.UtilityEquipment;
import db.UserSession;
import org.hibernate.Session;
import org.junit.Test;

import java.sql.Date;

/**
 * Created by gayashan on 9/13/2017.
 */
public class InventoryManagementTest {


    @Test
    public void canAddUtilityEquipment(){

        UtilityEquipment utilityEquipment = new UtilityEquipment();
        utilityEquipment.setEquipmentID(1);
        utilityEquipment.setLifetime(12);
        utilityEquipment.setCost(234.0);
        utilityEquipment.setPurchaseDate(new Date(System.currentTimeMillis()));

        Session session = UserSession.getSession();




        session.beginTransaction();
        session.save(utilityEquipment);
        session.getTransaction().commit();


    }


    @Test
    public void canAddMatine(){

        Machine utilityEquipment = new Machine();
        utilityEquipment.setEquipmentID(1);

        utilityEquipment.setCost(234.0);
        utilityEquipment.setPurchaseDate(new Date(System.currentTimeMillis()));

        Session session = UserSession.getSession();




        session.beginTransaction();
        session.save(utilityEquipment);
        session.getTransaction().commit();


    }


}
